//
//  ViewController.swift
//  check_wkWebview
//
//  Created by MinhNN18 on 2019/02/27.
//  Copyright © 2019 MinhNN18. All rights reserved.
//

import UIKit
import WebKit
import SafariServices

//Add sample comment 2
class myViewController: UIViewController, UIScrollViewDelegate, MailToDelegate, WKUIDelegate {
    
    
//    let viewController = UIApplication.shared.keyWindow!.rootViewController as! myViewController
    var subWebView = WKSubWebView()
    let mlabel = UILabel.init(frame: CGRect(x: 0, y: 40, width: UIScreen.main.bounds.width, height: 20))
//    var myPlayer: WKWebView!
    //MARK: Override
    override func loadView() {
        super.loadView()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let config = WKWebViewConfiguration()
        
        config.dataDetectorTypes = [.address]
        
        config.allowsInlineMediaPlayback = true
        config.mediaTypesRequiringUserActionForPlayback = []
//        config.allowsPictureInPictureMediaPlayback = true
//        config.ignoresViewportScaleLimits = true
        
        let processPool = WKProcessPool()
        config.processPool = processPool
        
        let contentController = WKUserContentController()
//        let scriptSource = "document.body.style.backgroundColor = `red`;"
//        let scriptSource = "window.webkit.messageHandlers.test.postMessage(`Hello, world!`);"
//        let scriptSource = "$( document ).ajaxSend(function( event, request, settings )  { callNativeApp (settings.data);}); function callNativeApp (data) {try { webkit.messageHandlers.callbackHandler.postMessage(data);} catch(err) {console.log('The native context does not exist yet'); }}"
//        let ajaxHandler = WKUserScript(source: scriptSource, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        
        
        
        
//        let jsHandler = String(describing: Bundle.main.url(forResource: "ajaxHandler", withExtension: "js"))
        let jsHandler = try? String(contentsOf: Bundle.main.url(forResource: "ajaxHandler", withExtension: "js")!, encoding: String.Encoding.utf8)
        let ajaxHandler = WKUserScript(source: jsHandler!, injectionTime: .atDocumentStart, forMainFrameOnly: false)
        contentController.add(subWebView, name: "handler")
        contentController.addUserScript(ajaxHandler)
        config.userContentController = contentController
        
        
        
//        NSString *jsHandler = [NSString stringWithContentsOfURL:[[NSBundle mainBundle]URLForResource:@"ajaxHandler" withExtension:@"js"] encoding:NSUTF8StringEncoding error:NULL];
//        WKUserScript *ajaxHandler = [[WKUserScript alloc]initWithSource:jsHandler injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
//        [userContentController addScriptMessageHandler:self name:@"callbackHandler"];
//        [userContentController addUserScript:ajaxHandler];
    
        
        
        subWebView = WKSubWebView(frame: self.view.frame, configuration: config)
        subWebView.allowsLinkPreview = true // 3D touch review
        subWebView.customUserAgent = "Mozilla/5.0 (iPhone;CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Version/12.2 Safari/9537.53 SecuredBrowser/com.i3-systems.ios.securedbrowser.i-FILTER"
        subWebView.navigationDelegate = subWebView
        subWebView.uiDelegate = subWebView
        subWebView.mailToDelegate = self
        subWebView.safariDelegate = self
        subWebView.allowsBackForwardNavigationGestures = true
        
        // Pull to refresh - Start
        subWebView.scrollView.bounces = true
        let refreshControl = UIRefreshControl()
        subWebView.scrollView.refreshControl = refreshControl
        refreshControl.addTarget(subWebView, action: #selector(subWebView.refreshWebView(sender:)), for: .valueChanged)
        // Pull to refresh - End
        
        view = subWebView
        
        self.buildToolBar()
        
        mlabel.text = ""
        mlabel.textColor = .blue
        mlabel.backgroundColor = .yellow
        mlabel.font = UIFont(name: mlabel.font.fontName, size: 12)
        view.addSubview(mlabel)
        
        
        // i3BROWSER 1747 - Long tap - Add start
        //Tap, Long tap
        // ジェスチャーを生成(今回はタップ・スワイプ・長押し)
//        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tap(_:)))
//        let swipeGesture:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipe(_:)))
//        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: subWebView, action: #selector(subWebView.tap(_:)))
//        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(_:)))
        
        // デリゲートをセット
//        tapGesture.delegate = self;
//        swipeGesture.delegate = self;
//        longPressGesture.delegate = self;
        
        // WebViewに追加
//        subWebView.addGestureRecognizer(tapGesture)
//        subWebView.addGestureRecognizer(swipeGesture)
//        subWebView.addGestureRecognizer(longPressGesture)
        // i3BROWSER 1747 - Long tap - Add end
        
        
//        myPlayer = WKWebView(frame: CGRect(x: 0, y: 0, width: 375, height: 300), configuration: config)
//        self.view.addSubview(myPlayer)
//        myPlayer.load(URLRequest(url: URL(string: "https://sample-videos.com/video123/mp4/240/big_buck_bunny_240p_2mb.mp4?playsinline=1")!))
        doRequest(subWebView.testURL[14])
//        subWebView.scrollView.setContentOffset(CGPoint(x: 0, y: -88.0), animated: false)
        
        
        
//        let videoURL = URL(string: "https://sample-videos.com/video123/mp4/240/big_buck_bunny_240p_2mb.mp4")
//        let player = AVPlayer(url: videoURL!)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.entersFullScreenWhenPlaybackBegins = false
//        playerViewController.exitsFullScreenWhenPlaybackEnds = true
//        playerViewController.player = player
//        self.present(playerViewController, animated: true) {
//            playerViewController.player!.play()
//        }
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    //MARK: Protocol
    
    func updateScreenWithMailTo(_ text: String) {
        mlabel.text = text
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateContentInsets()
    }
    
//    override func layoutSubviews() {
//        super layoutSubviews()
//        updateContentInsets()
//    }
    
    private func needsUpdateForContentInsets(_ insets: UIEdgeInsets) -> Bool {
        let scrollView = subWebView.scrollView
        return scrollView.contentInset.top != insets.top || scrollView.contentInset.bottom != insets.bottom
    }
    
    private func updateWebViewScrollViewInsets(_ insets: UIEdgeInsets) {
        let scrollView = subWebView.scrollView
        needsUpdateForContentInsets(insets)
        
        scrollView.scrollIndicatorInsets = insets
        scrollView.contentInset = insets
    }
    
    private func updateContentInsets() {
            updateWebViewScrollViewInsets(subWebView.scrollView.adjustedContentInset)
    }
  
    
    //MARK: UI
    
    func buildToolBar() -> Void {
        navigationController?.isToolbarHidden = false
        navigationController?.hidesBarsOnSwipe = false
//        var items = [UIBarButtonItem]()
//        items.append(UIBarButtonItem( title: "Back", style: .plain, target: self, action: #selector(subWebView.goBack)))
//        self.navigationController?.toolbar.items = items
//        let arr: [Any] = [items]
//        setToolbarItems(arr as? [UIBarButtonItem] ?? [UIBarButtonItem](), animated: true)
        
        let backItem    = UIBarButtonItem(title: "Back", style: .plain, target: subWebView, action: #selector(subWebView.goBack))
        toolbarItems    = [backItem]
        
        let reloadItem  = UIBarButtonItem(title: "Reload", style: .plain, target: subWebView, action: #selector(subWebView.reload))
        toolbarItems?.append(reloadItem)
        
        let googleItem  = UIBarButtonItem(title: "GG", style: .plain, target: self, action: #selector(goGoogle))
        let certItem    = UIBarButtonItem(title: "Cert", style: .plain, target: self, action: #selector(goCertDownloadAddress))
        let addressItem = UIBarButtonItem(title: "Address", style: .plain, target: self, action: #selector(ggoCheckAddress))
        let driveItem   = UIBarButtonItem(title: "Drive", style: .plain, target: self, action: #selector(goDrive))
        let videoItem = UIBarButtonItem(title: "Video", style: .plain, target: self, action: #selector(goVideo))
        
        toolbarItems?.append(googleItem)
        toolbarItems?.append(certItem)
        toolbarItems?.append(videoItem)
        toolbarItems?.append(addressItem)
//        toolbarItems?.append(driveItem)
        
    }

    
    
    
    //MARK: Request
    
    func doRequest(_ withString: String) -> Void {
//        let tURL = URL(string: subWebView.testURL[0])
        let tURL = URL(string: withString)
        subWebView.mRequest = NSURLRequest(url: tURL!) as URLRequest
        let _ = subWebView.load(subWebView.mRequest!)
    
    }
    
    @objc func ggoCheckAddress() {
        doRequest(subWebView.testURL[0])
    }
    @objc func goCertDownloadAddress() {
        doRequest(subWebView.testURL[14])
    }
    @objc func goGoogle() {
        doRequest(subWebView.testURL[3])
    }
    
    @objc func goDrive() {
        doRequest(subWebView.testURL[13])
    }
    
    @objc func goVideo() {
        doRequest(subWebView.testURL[5])
    }
    
    
    //MARK: gesture
    
    // i3BROWSER 1747 - Long tap - Add start
    func gestureRecognizer(
        _ gestureRecognizer: UIGestureRecognizer,
        shouldRecognizeSimultaneouslyWith
        otherGestureRecognizer: UIGestureRecognizer
        ) -> Bool {
        return true
    }
    
    @objc func tap(_ sender: UITapGestureRecognizer){
        //タップ時の処理
        print("tap")
//        perform(#selector(getInforFromActionSheet), with: self, afterDelay: 1.0)
    }
    
    @objc func swipe(_ sender: UITapGestureRecognizer){
        //スワイプ時の処理
        print("swipe")
    }
    
    @objc func longPress(_ sender: UITapGestureRecognizer){
        //長押し時の処理
        print("longPress")
//        perform(#selector(getInforFromActionSheet), with: self, afterDelay: 0.5)
    }
    

}

extension WKSubWebView {
    
    // i3BROWSER 1747 - Long tap - Add start
    @objc func getInforFromActionSheet() -> String {
        let m_window : UIWindow = UIApplication.shared.keyWindow!
        print("======== GOT VIEW =======")
        //        print(m_window.recursiveDescription)
        print(findTextRegex(regex: "\'(.*@{1}.*)\'", onString: m_window.recursiveDescription as String))
        self.mailToDelegate?.updateScreenWithMailTo(findTextRegex(regex: "\'(.*@{1}.*)\'", onString: m_window.recursiveDescription as String))
//        mlabel.text = findTextRegex(regex: "\'(.*@{1}.*)\'", onString: m_window.recursiveDescription as String)
        return findTextRegex(regex: "\'(.*@{1}.*)\'", onString: m_window.recursiveDescription as String)
    }
    
    
    private func findTextRegex(regex rxPattern: String, onString txtTarget: String) -> String {
        
        let range = NSRange(location: 0, length: txtTarget.count)
        let regex = try! NSRegularExpression(pattern: rxPattern, options: .caseInsensitive)
        let matchRange = regex.rangeOfFirstMatch(in: txtTarget, options: [], range: range)
        
        return String(txtTarget.substring(with: matchRange) ?? "Cannot find the matched string.")
        
    }
    // i3BROWSER 1747 - Long tap - Add end
    
}


// MARK: Non-Public API

//expr -l objc -O -- [[UIWindow keyWindow] recursiveDescription]
// i3BROWSER 1747 - Long tap - Add start
extension UIView {
    
    var recursiveDescription: NSString {
        return value(forKey: "recursiveDescription") as! NSString
    }
}
// i3BROWSER 1747 - Long tap - Add end



// MARK: SFSafariViewController

extension myViewController: SafariDelegate {
    
    func showSafari(_ withURL: String) {
        if let url = URL(string: withURL){
//            let config = SFSafariViewController.Configuration()
            let safariVc = SFSafariViewController(url: url)
            self.present(safariVc, animated: true, completion: nil)
        }
        
    }
    
//    public func showSafari(_ which: Int) {
//        if let url = URL(string: "https://www.hackingwithswift.com/read/\(which + 1)") {
//            let config = SFSafariViewController.Configuration()
//            config.entersReaderIfAvailable = true
//
//            let vc = SFSafariViewController(url: url, configuration: config)
//            self.present(vc, animated: true)
//        }
//    }
}

// MARK: WKNavigationDelegate
//extension myViewController : WKNavigationDelegate {
//
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        // Remember current request to load again in case of plainText/richText
//        subWebView.mRequest = navigationAction.request
//        switch navigationAction.request.url?.scheme {
//        case "mailto":
//            print(String.init(describing:navigationAction.request.mainDocumentURL))
//            subWebView.mailTo = "MAILTO :  " + String.init(describing:navigationAction.request.mainDocumentURL!)
//            subWebView.mailToDelegate?.updateScreenWithMailTo(subWebView.mailTo!)
//            subWebView.mLastURL = subWebView.mailTo!
//        default:
//            subWebView.mailTo = "URL =  " + String.init(describing:navigationAction.request.mainDocumentURL!)
//            subWebView.mailToDelegate?.updateScreenWithMailTo(subWebView.mailTo!)
//            subWebView.mLastURL = subWebView.mailTo!
//            break
//        }
//
//
//        // Check if it is POST request, then reload that request
//        // Now this way will send empty http post body. May prevent by following this link:
//        // https://stackoverflow.com/questions/28766676/how-can-i-monitor-requests-on-wkwebview
//        //                if navigationAction.request.httpMethod == "POST" {
//        //                    decisionHandler(.cancel)
//        //                    self.reloadURL(navigationAction.request)
//        //                    self.safariDelegate?.showSafari(mRequest!.url!.absoluteString) // This line will force app to show safari vc in case of POST request.
//        //                    return
//        //                }
//
//        decisionHandler(.allow)
//    }
//
//    private func reloadURL(_ withURL : URLRequest) {
//        let mutableRequest = (withURL as NSURLRequest).mutableCopy() as! NSMutableURLRequest
//        let customRequest = withURL
//        if (((customRequest.value(forHTTPHeaderField: "DEVICE_ID") == nil) || customRequest.value(forHTTPHeaderField: "SERIAL_NO") == nil)) {
//            print("MainDocumentURL     : \(String(describing: subWebView.mRequest?.mainDocumentURL)) ")
//            print("mRequest?.httpMethod: \(String(describing: subWebView.mRequest?.httpMethod))")
//            print("mRequest?.httpBody  : \(String(describing: subWebView.mRequest?.httpBody))")
//
//            mutableRequest.setValue("117F93EB-CECA-4C59-B7B7-9614B115A903", forHTTPHeaderField: "DEVICE_ID")
//            mutableRequest.setValue("FVFY297BHV22", forHTTPHeaderField: "SERIAL_NO")
//            mutableRequest.setValue("Mozilla/5.0 (iPhone;CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Version/12.2 Safari/9537.53 SecuredBrowser/com.i3-system.test.securedbrowser.i-Flm", forHTTPHeaderField: "User-Agent")
//            mutableRequest.setValue("en-jp", forHTTPHeaderField: "Accept-Language")
//            _ = subWebView.load(mutableRequest as URLRequest)
//        }
//    }
//
//    //Add start
//    private func findTextRegex(regex rxPattern: String, onString txtTarget: String) -> String {
//
//        let range = NSRange(location: 0, length: txtTarget.count)
//        let regex = try! NSRegularExpression(pattern: rxPattern, options: .caseInsensitive)
//        let matchRange = regex.rangeOfFirstMatch(in: txtTarget, options: [], range: range)
//
//        return String(txtTarget.substring(with: matchRange) ?? "Cannot find the matched string.")
//
//    }
//    //Add end
//
//    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
//        let httpResponse = navigationResponse.response as! HTTPURLResponse
//        let headers = httpResponse.allHeaderFields as NSDictionary
//        let contentType = headers["Content-Type"] as! String
//        subWebView.mType = self.findTextRegex(regex: "(text/plain)", onString: contentType)
//
//        // Download file - Mod start
//        if let contentPosition = headers["Content-Disposition"] as? String {
//            //            let range2 = NSRange(location: 0, length: contentPosition.count)
//            //            let regex2 = try! NSRegularExpression(pattern: String.init(describing:"([a-zA-Z|\0-9]*\\..{3,4})"), options: .caseInsensitive)
//
//            //            let range3 = NSRange(location: 0, length: contentPosition.count)
//            //            let regex3 = try! NSRegularExpression(pattern: String.init(describing:"(.{3,4}$)"), options: .caseInsensitive)
//
//
//            //            let matchRange2 = regex2.rangeOfFirstMatch(in: contentPosition, options: [], range: range2)
//            //            let matchRangePathExtension = regex3.rangeOfFirstMatch(in: contentPosition, options: [], range: range3)
//            //            self.pathEx = String(contentPosition.substring(with: matchRangePathExtension) ?? "")
//            //            mFileName = contentPosition.substring(with: matchRange2) ?? ""
//
//            subWebView.pathEx = self.findTextRegex(regex: "(.{3,4}$)", onString: contentPosition)
//            subWebView.mFileName = self.findTextRegex(regex: "([a-zA-Z|\0-9]*\\..{3,4})", onString: contentPosition)
//
//            print("FileName = \(subWebView.mFileName)")
//        }
//
//        if subWebView.mFileName.isEmpty {}
//        else {
//            decisionHandler(.cancel)
//            //            self.downloadFile(url: mRequest!.url!)
//            subWebView.safariDelegate?.showSafari(subWebView.mRequest!.url!.absoluteString) // i3BROWSER - 1715
//            return
//        }
//        // Download file - Mod end
//
//        //Find charset
//        //        if let charset = navigationResponse.response.textEncodingName {
//        //            print("TYPE: \(mType) - CHARSET: \(charset)")
//        //        } else {
//        //            print("TYPE: \(mType)")
//        //        }
//
//        decisionHandler(.allow)
//    }
//
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        print("Can go back = \(subWebView.canGoBack)")
//        if (String(subWebView.mType) == "text/plain" && subWebView.isEncoded == false) {
//            subWebView.loadmRequest()
//        }
//        subWebView.isEncoded = false
//
//        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()",
//                                   completionHandler: { (html: Any?, error: Error?) in
//                                    guard let html = html else {
//                                        return
//                                    }
//                                    print("MINH_HTML:\(String.init(describing: html))")
//        })
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
//            webView.scrollView.setZoomScale(self.zoomScale, animated: false)
//            webView.scrollView.setContentOffset(self.contentOffset, animated: false)
//        })
//    }
//
//    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//    }
//
//    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
//    }
//}


