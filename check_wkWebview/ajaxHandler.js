//Every time an Ajax call is being invoked the listener will recognize it and  will call the native app with the request details

//$( document ).ajaxSend(function( event, request, settings )  {
//    callNativeApp (settings.data);
//});
//
//function callNativeApp (data) {
//    try {
//        webkit.messageHandlers.handler.postMessage(data);
//    }
//    catch(err) {
//        console.log('The native context does not exist yet');
//    }
//}

//var open = XMLHttpRequest.prototype.open;
//XMLHttpRequest.prototype.open = function() {
//    this.addEventListener("load", function() {
//                          var message = {"status" : this.status, "responseURL" : this.responseURL, "responseData" : this.responseText}
//                          webkit.messageHandlers.handler.postMessage(message);
//                          });
//    open.apply(this, arguments);
//};

var form = document.querySelector("form");
form.onsubmit = (e) => {
    e.preventDefault();
    var formData = new FormData();
    var input = e.target.querySelector("input");
    webkit.messageHandlers.handler.postMessage(input);
    
    formData.append("file", input.files[0], input.files[0].name);

    var response = new Response(formData);
    var stream = response.body;
    var reader = stream.getReader();
    var decoder = new TextDecoder();
    reader.read()
    .then(function processData(result) {
          webkit.messageHandlers.handler.postMessage(response);

          if (result.done) {
          console.log("stream done");
          webkit.messageHandlers.handler.postMessage(response);
          return;
          }
          var data = decoder.decode(result.value);
          console.log(data);
          webkit.messageHandlers.handler.postMessage(response);
          return reader.read().then(processData);
          })
    .catch(function(err) {
           console.log("catch stream cancellation:", err);
           });

    reader.closed.then(function() {
                       console.log("stream closed");
                       webkit.messageHandlers.handler.postMessage(response);
                       });
}
