//
//  WKSubWebView.swift
//  check_wkWebview
//
//  Created by MinhNN18 on 2019/04/01.
//  Copyright © 2019 MinhNN18. All rights reserved.
//

import UIKit
import WebKit
import SafariServices


protocol MailToDelegate {
    func updateScreenWithMailTo(_ text: String) -> Void
}

// i3BROWSER - 1715 A_Start
protocol SafariDelegate {
    func showSafari(_ withURL: String) -> Void
}
// i3BROWSER - 1715 A_End

class WKSubWebView: WKWebView, URLSessionTaskDelegate, URLSessionDownloadDelegate, URLSessionDelegate, WKScriptMessageHandler {
    

    
    /* [2019_05_07][https://insight.fsoft.com.vn/jira3/browse/I3BROWSER-1312] - Showing desktop ver after encoded - Add Start*/
    let sessionConfiguration = URLSessionConfiguration.default
    /* [2019_05_07][https://insight.fsoft.com.vn/jira3/browse/I3BROWSER-1312] - Showing desktop ver after encoded - Add End*/
    
    var mRequest: URLRequest?
    var dataTask: URLSessionDataTask?
    var mailTo: String?
    var mailToDelegate: MailToDelegate?
    var safariDelegate: SafariDelegate? // i3BROWSER - 1715
    var mLastURL: String?
    
    var m_customRequest: URLRequest?
    
    public let groupID: String
    public let portalID = "downnloadedFile"
    public var pathEx: String
    
    public var coordinator: NSFileCoordinator? // For writing dict on the file system
    public var portalQueue = OperationQueue() // For writing dict on the file system
    
    public var viewScale: CGFloat?
    public let testURL = ["http://asebi.phpapps.jp/",
                          "https://www.jrhakatacity.com/access/guide.html",
                          "http://onlinedemo.cybozu.co.jp:8080/remote/login.vm",
                          "https://file-examples.com/index.php/sample-video-files/sample-mp4-files/",
                          "https://www.google.com",
                          "https://sample-videos.com/",
                          "https://file-examples.com/wp-content/uploads/2017/04/file_example_MP4_640_3MG.mp4?playsinline=1",
                          "https://sample-videos.com/video123/mp4/240/big_buck_bunny_240p_2mb.mp4?playsinline=1?autoplay=0",
                          "https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_link_mailto",
                          "https://tinhte.vn/threads/share-760-giao-trinh-tin-hoc-tieng-anh-dropbox-dap-le-ban-hminh2008.1108958/page-4",
                          "https://www2.sproxy.jp/download/i3-test.com",
                          "https://rc2.clomo.com/portal/blue-desk.com/",
                          "https://mail.google.com/mail",
                          "https://www2.sproxy.jp/download/i3-test.com",
                          "https://drive.google.com/drive/my-drive",
                          "https://www2.sproxy.jp/download/i3-test.com",
                          "https://docs.microsoft.com/en-us/azure/devops/pipelines/get-started/what-is-azure-pipelines?view=azure-devops",
                          ""]
    
    var mType: String = ""
    var mFileName: String = ""
    weak var delegate: WKNavigationDelegate?
    var isEncoded = false
    let localURL = ""
    
    var mCookieValue: String = ""
    
   
    override init(frame: CGRect, configuration: WKWebViewConfiguration) {
        groupID = "group.com.clomo.apps.ios"
        pathEx = "txt"
        super .init(frame: CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: frame.size.height), configuration: configuration)
        
//        self.isUserInteractionEnabled = true
//        let interaction = UIContextMenuInteraction(delegate: self)
//        self.addInteraction(interaction)
        
        // Auto scroll down - S
        
        scrollView.contentInset = UIEdgeInsets(top: 88.0, left: 00.0, bottom: 0.0, right: 0.0)
        scrollView.contentInsetAdjustmentBehavior = .automatic
//        print("scrollView.adjustedContentInset.top= \(scrollView.adjustedContentInset.top)")
//        scrollView.setContentOffset(CGPoint(x: 0.0, y: -44.0), animated: true)
//        scrollView.contentOffset = CGPoint(x: 0.0, y: 44.0)
        
        
//        if #available(iOS 13.0, *) {
//            scrollView.automaticallyAdjustsScrollIndicatorInsets = true
//        } else {
            // Fallback on earlier versions
//        }
        // Auto scroll down - E
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: _LOAD CAN BACKFORWARD_
    
    @objc override func load(_ request: URLRequest?) -> WKNavigation? {
    delegate = self
    self.uiDelegate = self
    isEncoded = false
    
    guard let customRequest = request else { return nil }
    let mutableRequest = (customRequest as NSURLRequest).mutableCopy() as! NSMutableURLRequest
    if ((customRequest.value(forHTTPHeaderField: "DEVICE_ID") == nil) ||
        customRequest.value(forHTTPHeaderField: "SERIAL_NO") == nil) {
//        mutableRequest.setValue("117F93EB-CECA-4C59-B7B7-9614B115A903", forHTTPHeaderField: "DEVICE_ID")
//        mutableRequest.setValue("FVFY297BHV22", forHTTPHeaderField: "SERIAL_NO")
//        mutableRequest.setValue("Mozilla/5.0 (iPhone;CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Version/12.2 Safari/9537.53 SecuredBrowser/com.i3-system.test.securedbrowser.i-Flm", forHTTPHeaderField: "User-Agent")
//        mutableRequest.setValue("en-jp", forHTTPHeaderField: "Accept-Language")
        
        
            mutableRequest.setValue("308E56F1-4DEA-4EF5-A093-76FD27706F5F", forHTTPHeaderField: "DEVICE_ID")
            mutableRequest.setValue("C6KXLPPYKXKX", forHTTPHeaderField: "SERIAL_NO")
            mutableRequest.setValue("Mozilla/5.0 (iPhone;CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Version/12.2 Safari/9537.53 SecuredBrowser/com.i3-systems.ios.securedbrowser.i-FILTER", forHTTPHeaderField: "User-Agent")
            mutableRequest.setValue("ja-jp", forHTTPHeaderField: "Accept-Language")
        
    }
    super.load(mutableRequest  as URLRequest)
    
    return nil
}
    
    @objc func loadmRequest() -> Void {
        loadSpecialURL(mRequest!)
    }
    
    func failCase() -> Void {
        DispatchQueue.main.async {
            //TODO
        }
    }
    
    func loadSpecialURL(_ request: URLRequest) -> Void {
        self.loadURL(request, success: { (newRequest, response, data) in
            if let data = data, let response = response {
               let _ = self.webViewLoad(data: data, response: response)
            }
        }, failure: failCase)
    }

    func loadURL(_ request: URLRequest, success: @escaping (URLRequest, HTTPURLResponse?, Data?) -> Void, failure: @escaping () -> Void) {
        
        dataTask?.cancel()
        
        guard let url = request.url else { return }
        
        /* [2019_05_07][https://insight.fsoft.com.vn/jira3/browse/I3BROWSER-1312] - Showing desktop ver after encoded - Modify Start*/
        sessionConfiguration.httpAdditionalHeaders = ["User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148"]
        let sesion = URLSession(configuration: sessionConfiguration)

        let dataTask = sesion.dataTask(with: url) { data, response, error in
        /* [2019_05_07][https://insight.fsoft.com.vn/jira3/browse/I3BROWSER-1312] - Showing desktop ver after encoded - Modify End*/
            
            // Set dataTask to nil after done
            defer { self.dataTask = nil }
            if let error = error {
//                print("ERROR: \(error.localizedDescription)")
                failure()
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                success(request, response, data)
            }
        }
        dataTask.resume()
    }
    
    func webViewLoad(data: Data, response: URLResponse) -> WKNavigation? {
        guard let url = response.url else {
            return nil
        }
        //let encode = response.textEncodingName ?? "Shift-jis"
        let encode = "Shift-jis"
        let mime = response.mimeType ?? "text/plain"
        isEncoded = true
        DispatchQueue.main.async {
            _ = self.load(data, mimeType: mime, characterEncodingName: encode, baseURL: url)!
        }
        return nil
    }
    

   
    //MARK: _OVERRIDE_
    override func goBack() -> WKNavigation? {
        if self.canGoBack {
            return super.goBack()
        }
        return nil
    }
    
    override func reload() -> WKNavigation? {
//        guard let request = mRequest else {
//            return nil
//        }
//        self.scrollView.scrollsToTop = true
        
        super.reloadFromOrigin()
        scrollView.contentInsetAdjustmentBehavior = .never
//        self.evaluateJavaScript("window.scrollTo(0,0)", completionHandler: nil)
//        scrollView.contentInset = UIEdgeInsets(top: 44.0, left: 0.0, bottom: 0.0, right: 0.0)
//        print("scrollView.adjustedContentInset.top= \(scrollView.adjustedContentInset.top)")
        return nil
//        return self.load(request)
    }
    
    @objc func refreshWebView(sender: UIRefreshControl) {
//        _ = self.reload()
        sender.endRefreshing()
        scrollView.contentInsetAdjustmentBehavior = .never
//        self.evaluateJavaScript("window.scrollTo(44,0)", completionHandler: nil)
//        scrollView.contentInset = UIEdgeInsets(top: 44.0, left: 0.0, bottom: 0.0, right: 0.0)
//        print("scrollView.adjustedContentInset.top= \(scrollView.adjustedContentInset.top)")
    }
    
    // JAVASCRIPT HANDLE MESSAGE
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if let messagebody = message.body as? Any {
            print("Javascript message: \(messagebody)")
        }
    }
    
    
    // MARK: TAP
    
    @objc func tap(_ sender: UITapGestureRecognizer){
        //タップ時の処理
        print("WK tap")
    }
    
}




// MARK: UIContextMenuInteractionDelegate
//extension WKSubWebView : UIContextMenuInteractionDelegate {
    //MARK: CONTEXT
    // Does not run
//    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        //
        //        let actionProvider: ([UIMenuElement]) -> UIMenu? = { _ in
        //            let share = UIAction(__title: "Share", image: UIImage(systemName: "square.and.arrow.up"), identifier: UIAction.Identifier(rawValue: "share")) { _ in
        //                // some action
        //            }
        //            let editMenu: UIMenu = {
        //                let copy = UIAction(__title: "Copy", image: nil, identifier: UIAction.Identifier(rawValue: "copy")) { _ in
        //                    // some action
        //                }
        //                let delete = UIAction(__title: "Delete", image: UIImage(systemName: "trash"), identifier: UIAction.Identifier("delete")) { _ in
        //                    // some action
        //                }
        //                delete.attributes = [.destructive]
        //                return UIMenu(__title: "Edit..", image: nil, identifier: nil, children: [copy, delete])
        //            }()
        //
        //            return UIMenu(__title: "Edit..", image: nil, identifier: nil, children: [share, editMenu])
        //        }
        //
//        return UIContextMenuConfiguration(identifier: nil,
//                                          previewProvider: nil,
//                                          actionProvider: nil)
//    }
//}




// MARK: WKNavigationDelegate
extension WKSubWebView : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        // Remember current request to load again in case of plainText/richText
        mRequest = navigationAction.request
//        print("Request: " + String(describing: navigationAction.request.bodySteamAsJSON()))
        print("X_Request: " + String(describing: navigationAction.request))
        
        
        m_customRequest = navigationAction.request
        let mutableRequest = (m_customRequest as! NSURLRequest).mutableCopy() as! NSMutableURLRequest

        if ((m_customRequest!.value(forHTTPHeaderField: "DEVICE_ID") == nil) ||
            m_customRequest!.value(forHTTPHeaderField: "SERIAL_NO") == nil) {
            decisionHandler(.cancel)

            mutableRequest.setValue("308E56F1-4DEA-4EF5-A093-76FD27706F5F", forHTTPHeaderField: "DEVICE_ID")
            mutableRequest.setValue("C6KXLPPYKXKX", forHTTPHeaderField: "SERIAL_NO")
            mutableRequest.setValue("Mozilla/5.0 (iPhone;CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Version/12.2 Safari/9537.53 SecuredBrowser/com.i3-systems.ios.securedbrowser.i-FILTER", forHTTPHeaderField: "User-Agent")
            mutableRequest.setValue("en-jp", forHTTPHeaderField: "Accept-Language")

            self.load(mutableRequest as URLRequest)

            return
        }
        
        
        
        
        switch navigationAction.request.url?.scheme {
        case "mailto":
            print(String.init(describing:navigationAction.request.mainDocumentURL))
            mailTo = "MAILTO :  " + String.init(describing:navigationAction.request.mainDocumentURL!)
            mailToDelegate?.updateScreenWithMailTo(mailTo!)
            mLastURL = mailTo!
        default:
            mailTo = "URL =  " + String.init(describing:navigationAction.request.mainDocumentURL!)
            mailToDelegate?.updateScreenWithMailTo(mailTo!)
            mLastURL = mailTo!
            break
        }
        
//        if navigationAction.request.httpMethod == "POST" {
//            if (navigationAction.request.httpBody != nil) {
//                print("HTTP body is not NULL : " + String(describing: navigationAction.request.httpBody))
//            }
//
//            let javascriptPOSTRedirect: String = "var form = document.createElement('form');form.method = 'POST';form.action = '<URL>';var input = document.createElement('input');input.type = 'text';input.name = '<key>'; input.value = '<value>';form.appendChild(input); form.submit();"
//
//            webView.evaluateJavaScript(javascriptPOSTRedirect) { (content, error) in
//                print(content)
//            }
//        }

        
        switch navigationAction.navigationType {
        case  .formSubmitted:
//            print("This is a FromSubmmit request " + String(describing: navigationAction.request))
            mRequest = (navigationAction.request as NSURLRequest).mutableCopy() as! URLRequest
//            print(mRequest?.httpBody)
            print("SourceFrame request body " + String(describing: navigationAction.sourceFrame.request.httpBody))
            print(navigationAction.accessibilityElements)
        default:
            break
        }
        
//        if String(describing: navigationAction.request.mainDocumentURL!) == "http://onlinedemo.cybozu.co.jp:8080/remote/certificateMasterDetail.vm" {
//            print(navigationAction.request.allHTTPHeaderFields!)
//        }
        
//        if navigationAction.request.httpMethod == "POST" {
//            print("This is a POST request" + String(describing: navigationAction.request))
//            mRequest = (navigationAction.request as NSURLRequest).mutableCopy() as! URLRequest
//            print(mRequest)
//        }

        decisionHandler(.allow)
    }
    
    private func reloadURL(_ withURL : URLRequest) {
        let mutableRequest = (withURL as NSURLRequest).mutableCopy() as! NSMutableURLRequest
        let customRequest = withURL
        if (((customRequest.value(forHTTPHeaderField: "DEVICE_ID") == nil) || customRequest.value(forHTTPHeaderField: "SERIAL_NO") == nil)) {
//            print("MainDocumentURL     : \(String(describing: mRequest?.mainDocumentURL)) ")
//            print("mRequest?.httpMethod: \(String(describing: mRequest?.httpMethod))")
//            print("mRequest?.httpBody  : \(String(describing: mRequest?.httpBody))")
            
            mutableRequest.setValue("117F93EB-CECA-4C59-B7B7-9614B115A903", forHTTPHeaderField: "DEVICE_ID")
            mutableRequest.setValue("FVFY297BHV22", forHTTPHeaderField: "SERIAL_NO")
            mutableRequest.setValue("Mozilla/5.0 (iPhone;CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Version/12.2 Safari/9537.53 SecuredBrowser/com.i3-system.test.securedbrowser.i-Flm", forHTTPHeaderField: "User-Agent")
            mutableRequest.setValue("en-jp", forHTTPHeaderField: "Accept-Language")
            _ = load(mutableRequest as URLRequest)
        }
    }
    
    //Add start
    private func findTextRegex(regex rxPattern: String, onString txtTarget: String) -> String {
        
        let range = NSRange(location: 0, length: txtTarget.count)
        let regex = try! NSRegularExpression(pattern: rxPattern, options: .caseInsensitive)
        let matchRange = regex.rangeOfFirstMatch(in: txtTarget, options: [], range: range)
        
        return String(txtTarget.substring(with: matchRange) ?? "Cannot find the matched string.")
        
    }
    //Add end
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        let httpResponse = navigationResponse.response as! HTTPURLResponse
        let headers = httpResponse.allHeaderFields as NSDictionary
        let contentType = headers["Content-Type"] as! String
        mType = self.findTextRegex(regex: "(text/plain)", onString: contentType)
        
        // Download file - Mod start
        if let contentPosition = headers["Content-Disposition"] as? String {
            
            self.pathEx = self.findTextRegex(regex: "(.{3,4}$)", onString: contentPosition)
            mFileName = self.findTextRegex(regex: "([a-zA-Z|\0-9]*\\..{3,4})", onString: contentPosition)
            
            print("FileName = \(mFileName)")
        }
        
        if mFileName.isEmpty {}
        else {
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: httpResponse.allHeaderFields as! [String : String], for: httpResponse.url!)
            for cookie in cookies {
                HTTPCookieStorage.shared.setCookie(cookie)
            }
            decisionHandler(.cancel)
            self.usingNormalDataTaskWithRequest(mRequest!)
//            self.downloadFile(url: mRequest!.url!) //
//            self.safariDelegate?.showSafari(mRequest!.url!.absoluteString) // i3BROWSER - 1715
            return
        }
        

        
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        if mRequest?.httpMethod == "POST" {
            print("POST body = " + String(describing: mRequest?.httpBody))
            let _ = mRequest?.httpBody
        }
        
//        print("Can go back = \(canGoBack)")
       
        if (String(mType) == "text/plain" && isEncoded == false) {
            loadmRequest()
        }
        isEncoded = false
        
        
        
        // Get webpage HTML
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()",
                                   completionHandler: { (html: Any?, error: Error?) in
                                    guard let html = html else {
                                        return
                                    }
                                    print("MINH_HTML:\(String(describing: html))")
        })
        
        
        
//        webView.evaluateJavaScript("(function() { return document.cookie })()", completionHandler: { (response, error) -> Void in
//            let cookie = response as! String
//            let request = (self.mRequest! as NSURLRequest).mutableCopy() as! NSMutableURLRequest
//            request.setValue(cookie, forHTTPHeaderField:  "Cookie")
            
//            URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
//                // Your CSV file will be in the response object
//            }).resume()
            
//            self.usingNormalDataTaskWithRequest(request as! URLRequest)
//        })
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
//            webView.scrollView.setZoomScale(webView.scrollView.zoomScale, animated: false)
//            webView.scrollView.setContentOffset(webView.scrollView.contentOffset, animated: false)
//        })
        
//        print("scrollView.adjustedContentInset.top= \(scrollView.adjustedContentInset.top)")
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
        print(String(describing: navigation))
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
    }
}




//MARK: DOWNLOAD
extension WKSubWebView {
    
    // Download file - Add start
    func downloadFile(url: URL) {
        // DOWNLOAD TASK
//        self.usingDownloadTask(url)
        // POST
        self.usingNormalDataTask(url)
        
    }
    
    
    //Utility
    private func containerURL() -> URL? {
        //return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: groupID)
        return FileManager.default.temporaryDirectory
    }
    
    
    private func fileURL() -> URL? {
        return containerURL()?.appendingPathComponent(portalID).appendingPathExtension("txt")
    }
    
    
    private func usingDownloadTask(_ withUrl : URL) {
        dataTask?.cancel()
        let downloadSession = URLSession.init(configuration: .default, delegate: self, delegateQueue: OperationQueue.main)
        //======
        let task = downloadSession.downloadTask(with: withUrl) { localURL, urlResponse, error in
            defer {self.dataTask = nil}
            if let error = error {
                self.mailToDelegate?.updateScreenWithMailTo("Error" + String.init(describing:error))
            } else {
//                print("Saved to: " + String.init(describing: localURL!))
                self.mailToDelegate?.updateScreenWithMailTo("Saved to: " + String.init(describing: localURL!))
            }
        }
        task.resume()
        //======
    }
    
    
    private func usingDownloadTasksWithRequest(_ withRequest : URLRequest) {
        self.dataTask?.cancel()
        let downloadSession = URLSession.init(configuration: .default, delegate: self, delegateQueue: OperationQueue.main)
        let downloadTask = downloadSession.downloadTask(with: mRequest!) { (localURL, urlResponse, error) in
            defer{ self.dataTask = nil }
            if error != nil {
                return
            }
            if let response = urlResponse {
//                print("Got response: ")
//                print(response)
            }
//            print("Saved to: " + String.init(describing: localURL!))
            self.mailToDelegate?.updateScreenWithMailTo("Saved to: " + String.init(describing: localURL!))
        }
        downloadTask.resume()
    }
    
    
    private func usingNormalDataTask(_ withUrl : URL) {
        let parameterDictionary = ["password" : ""]
        var request = URLRequest(url: withUrl)
//        request.httpMethod = "POST"
        request.httpMethod = "GET"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.httpShouldHandleCookies = true
//        print("GETRequest: \(request) ")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return
        }
        request.httpBody = httpBody
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["User-Agent": "Mozilla/5.0 (iPhone;CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Version/12.2 Safari/9537.53 SecuredBrowser/com.i3-system.ios.securedbrowser.i-FILTER"]
        self.dataTask?.cancel()
        let session = URLSession(configuration: sessionConfiguration)
        
        session.dataTask(with: request) { (data, response, error) in
            defer{ self.dataTask = nil }
            if let response = response {
//                print("Got response: ")
//                print(response)
            }
            if let data = data {
                do {
                    print(String(data: data, encoding: String.Encoding.utf8) as Any)
                    self.saveFile(data)
                }
                catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    
    private func usingNormalDataTaskWithRequest(_ withRequest : URLRequest) {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["User-Agent": "Mozilla/5.0 (iPhone;CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 !(KHTML, like Gecko) Mobile/15E148 Version/12.2 Safari/9537.53 SecuredBrowser/com.i3-system.ios.securedbrowser.i-FILTER"]
//        sessionConfiguration.httpAdditionalHeaders = ["Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"]
//        sessionConfiguration.httpAdditionalHeaders = ["Content-Type": "application/x-www-form-urlencoded"]
//        sessionConfiguration.httpAdditionalHeaders = ["Origin": "http://onlinedemo.cybozu.co.jp:8080"]
//        sessionConfiguration.httpAdditionalHeaders = ["Referer": "http://onlinedemo.cybozu.co.jp:8080/remote/certificateMasterDetail.vm"]
        self.inputCookieToRequestInNormalDataTask(withRequest, method: "POST", sessionConfiguration)
    }
    
    
    private func inputCookieToRequestInNormalDataTask (_ withRequest: URLRequest, method withMethod: String, _ withConfig: URLSessionConfiguration){
        
        let postBody = "action=downloadCertificate&download=%E3%83%80%E3%82%A6%E3%83%B3%E3%83%AD%E3%83%BC%E3%83%89%E3%81%99%E3%82%8B"
        
        var c_request = URLRequest(url: withRequest.url!)
        c_request.httpMethod = withMethod
        c_request.httpBody = postBody.data(using: String.Encoding.ascii)
//        c_request.httpBody = mRequest?.httpBody
        c_request.setValue(String(describing: postBody.count), forHTTPHeaderField: "Content-length")
        WKWebsiteDataStore.default().httpCookieStore.getAllCookies { (cookies) in
            for cookie in cookies {
                if cookie.domain == "onlinedemo.cybozu.co.jp" {
                    self.configuration.websiteDataStore.httpCookieStore.setCookie(cookie)
                    HTTPCookieStorage.shared.setCookie(cookie)
                    print("//1 Got CookieAllVal: \(cookie)")
                    print("//1 Got CookieValues: \(cookie.value)")
                    self.mCookieValue = cookie.value
                    break
                }
            }
            
            c_request.setValue("JSESSIONID=" + self.mCookieValue, forHTTPHeaderField: "Cookie")
            self.normalDataTaskWithCookieRequest(withConfig, c_request)
            //                    self.usingDownloadTasksWithRequest(c_request)
        }
    }
    
    
    private func  normalDataTaskWithCookieRequest(_ withConfig : URLSessionConfiguration, _ withRequest : URLRequest) {
        self.dataTask?.cancel()
        let a_session =  URLSession.init(configuration: withConfig, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = a_session.dataTask(with: withRequest) { data, response, error in
            defer {self.dataTask = nil}
            if let response = response {
                print("Got response: ")
                let fullSessionId = self.findTextRegex(regex: "(JSESSIONID\\=[\\d|\\w]*)", onString: String(describing: response))
                let sessionId = self.findTextRegex(regex: "[\\d|\\w]+$", onString: fullSessionId)
                print(sessionId)
            }
            if let data = data {
                do {
                    print("★ GotData: ")
                    print("★ Data description : \(data.description)")
                    print(String(decoding: data, as: UTF8.self))
                    self.saveFile(data)
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    
    
    
    
    
    private func usingJavascript(_ withURL : URL) {
        //
    }
    
    private func saveFile (_ data : Data) {
        if let savedUrl = self.fileURL() {
            print("Saved to:" + savedUrl.absoluteString)
            if self.fileExists() == false {
                FileManager.default.createFile(atPath: savedUrl.path, contents: nil, attributes: nil) //createFile()
            } else {return}
            
            self.portalQueue.addOperation {
                [weak self] in
                var error: NSError?
                
                self?.coordinator?.coordinate(writingItemAt: savedUrl, options: .forReplacing, error: &error, byAccessor: { (savedUrl) in
                    let dataToSave: String  = String(data: data, encoding: .utf8)!
                    try? NSKeyedArchiver.archivedData(withRootObject: dataToSave, requiringSecureCoding: false).write(to: savedUrl)
                })
            }
        }
    }
    
    private func fileExists() -> Bool {
        return FileManager.default.fileExists(atPath: self.fileURL()!.path)
    }
    
    
    
    
    
    // Download file - Add start
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if totalBytesExpectedToWrite > 0 {
            let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
            mailToDelegate?.updateScreenWithMailTo("Progress \(downloadTask) \(progress)")
            print("Progress \(downloadTask) \(progress)")
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        mailToDelegate?.updateScreenWithMailTo("Download finished: \(location)")
        print("Download finished: \(location)")
        try? FileManager.default.removeItem(at: location)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        mailToDelegate?.updateScreenWithMailTo("Task completed: \(String.init(describing: task)), error: \(String.init(describing: error))")
        //        print("Task completed: \(task), error: \(error)")
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        mailToDelegate?.updateScreenWithMailTo("Session finished !")
    }
    // Download file - Add end

    
}




// MARK: WKUIDelegate
extension WKSubWebView : WKUIDelegate {
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        print("EndScroll scrollView.adjustedContentInset.top= \(scrollView.adjustedContentInset.top)")
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        //Handle case targetFrame = "_blank"
        if !(navigationAction.targetFrame?.isMainFrame == false) {
            webView.load(navigationAction.request)
        }
        return nil
    }
    
//MARK: iOS13 
//        func webView(_ webView: WKWebView, contextMenuConfigurationForElement elementInfo: WKContextMenuElementInfo, completionHandler: @escaping (UIContextMenuConfiguration?) -> Void) {
//            print(#function)
//
//            let actionProvider: ([UIMenuElement]) -> UIMenu? = { _ in
//                let share = UIAction(__title: "Share", image: UIImage(systemName: "square.and.arrow.up"), identifier: UIAction.Identifier(rawValue: "share")) { _ in
//                    // some action
//                }
//                let editMenu: UIMenu = {
//                    let copy = UIAction(__title: "Copy", image: nil, identifier: UIAction.Identifier(rawValue: "copy")) { _ in
//                        // some action
//                    }
//                    let delete = UIAction(__title: "Delete", image: UIImage(systemName: "trash"), identifier: UIAction.Identifier("delete")) { _ in
//                        // some action
//                    }
//                    delete.attributes = [.destructive]
//                    return UIMenu(__title: "Edit..", image: nil, identifier: nil, children: [copy, delete])
//                }()
//                
//                return UIMenu(__title: "Edit..", image: nil, identifier: nil, children: [share, editMenu])
//            }
//            
//            completionHandler(UIContextMenuConfiguration(identifier: nil,
//                                                         previewProvider: nil,
//                                                         actionProvider: actionProvider))
//            
//    }

    
//
//    func webView(_ webView: WKWebView, contextMenuDidEndForElement elementInfo: WKContextMenuElementInfo) {
//        print(#function)
//        print(elementInfo.linkURL as Any)
//    }
//
//    func webView(_ webView: WKWebView, contextMenuWillPresentForElement elementInfo: WKContextMenuElementInfo) {
//        print(#function)
//        print(elementInfo.linkURL as Any)
//
//        perform(#selector(self.getInforFromActionSheet), with: self, afterDelay: 0.2)
//    }
//
//    func webView(_ webView: WKWebView, contextMenuForElement elementInfo: WKContextMenuElementInfo, willCommitWithAnimator animator: UIContextMenuInteractionCommitAnimating) {
//        print(#function)
//        print(elementInfo.linkURL as Any)
//    }
    
    //webView(_:shouldPreviewElement:)
//    @objc func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
//        print("====== LINK: \(String(describing: elementInfo.linkURL!))")
//        mailToDelegate?.updateScreenWithMailTo(String(describing: elementInfo.linkURL!))
//        return true
//    }
    
    
    
    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController) {
        //        if previewingViewController is WebPreviewViewController {
        //            self.present(previewingViewController, animated: true) { }
        //        }
    }
    
    //webView(_:previewingViewControllerForElement:defaultActions:)
    @objc public func webView(_ webView: WKWebView, previewingViewControllerForElement elementInfo: WKPreviewElementInfo, defaultActions previewActions: [WKPreviewActionItem]) -> UIViewController? {
        guard let url = elementInfo.linkURL else { return nil }
        print("\(type(of: self)) \(#function)  url:\(url)")
        
        var actions = [WKPreviewActionItem]()
        // Peekで表示しているページに対するアクションをview controllerへ渡します。
        // 表示するアクションを選択することもできます。
        // Just set all actions like
        // actions = previewActions
        // or filter any actions
        for action in previewActions {
            switch action.identifier {
            case WKPreviewActionItemIdentifierOpen:
                actions.append(action)
            case WKPreviewActionItemIdentifierAddToReadingList:
                actions.append(action)
            case WKPreviewActionItemIdentifierCopy:
                actions.append(action)
            case WKPreviewActionItemIdentifierShare:
                actions.append(action)
            default: break
            }
        }
        
        // アクションリストを表示したいのでUIViewControllerのサブクラスを使っています
        let vc = WebPreviewViewController(url: url, actions: actions)
        
        vc.preferredContentSize = vc.view.bounds.size
        
        return nil
    }
    
}




// MARK: UIScrollViewDelegate
extension WKSubWebView : UIScrollViewDelegate {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("Scale value = \(scale)")
        self.viewScale = scale
    }
}



//MARK: _String Extension_
extension String {
    func substring(with nsrange: NSRange) -> Substring? {
        guard let range = Range(nsrange, in: self) else { return nil }
        return self[range]
    }
}


extension WKSubWebView {
    func load(_ request: URLRequest, with cookies: [HTTPCookie]) {
        var request = request
        let headers = HTTPCookie.requestHeaderFields(with: cookies)
        for (name, value) in headers {
            request.addValue(value, forHTTPHeaderField: name)
        }
       _ = load(request)
    }
}

extension WKSubWebView {
    func scrollViewDidChangeAdjustedContentInset(_ scrollView: UIScrollView) {
//        print("Did changed the scroll view")
        scrollView.contentInsetAdjustmentBehavior = .never
    }
}

extension URLRequest {
    
    func bodySteamAsJSON() -> Any? {
        
        guard let bodyStream = self.httpBodyStream else { return nil }
        
        bodyStream.open()
        
        // Will read 16 chars per iteration. Can use bigger buffer if needed
        let bufferSize: Int = 16
        
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bufferSize)
        
        var dat = Data()
        
        while bodyStream.hasBytesAvailable {
            
            let readDat = bodyStream.read(buffer, maxLength: bufferSize)
            dat.append(buffer, count: readDat)
        }
        
        buffer.deallocate()
        
        bodyStream.close()
        
        do {
            return try JSONSerialization.jsonObject(with: dat, options: JSONSerialization.ReadingOptions.allowFragments)
        } catch {
            
            print(error.localizedDescription)
            
            return nil
        }
    }
}
