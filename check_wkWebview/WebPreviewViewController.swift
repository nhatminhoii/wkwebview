//
//  WebPreviewViewController.swift
//  check_wkWebview
//
//  Created by MinhNN18 on 2019/07/10.
//  Copyright © 2019 MinhNN18. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class WebPreviewViewController: UIViewController {
    
    var webViewPreviewActionItems: [WKPreviewActionItem] = []
    var linkURL: URL?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(url: URL, actions: [WKPreviewActionItem]) {
        self.init(nibName: nil, bundle: nil)
        self.linkURL = url
        self.webViewPreviewActionItems = actions
    }
    
    // 受け取ったアクションのリストを表示するため
    override var previewActionItems: [UIPreviewActionItem] {
        get {
            return self.webViewPreviewActionItems
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Peek & Pop用のWKWebViewを表示
        if let _ = self.linkURL {
//            let web = WKWebView()
//            web.frame = self.view.bounds
//            self.view.addSubview(web)
//            web.load(URLRequest(url: url))
            print("OPEN MAILER with: \(self.linkURL!)")
        }
    }
    
}
